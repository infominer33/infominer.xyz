---
layout: page
title: "John Wolpert of ConsenSys talks awesomeness at Consensus 2018 CoinDeskLIVE"
description: "John Wolpert and Pete Rizzo Discuss the Weakening Distinciton Between Private and Public Blockchain."
cannonical_url: 'https://sourcecrypto.pub/transcripts/JohnWolpert-Consensus2018/'
redirect_to: https://sourcecrypto.pub/transcripts/JohnWolpert-Consensus2018/
permalink: transcripts/JohnWolpert-Consensus2018/
redirect_from: 
  - bitcoin-history/transcripts/JohnWolpert-Consensus2018
  - bitcoin-history/transcripts/JohnWolpert-Consensus2018/
---

