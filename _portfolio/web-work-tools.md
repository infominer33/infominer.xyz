---
layout: post
title:  Web-work.tools
subtitle: Digital Tools for a Digital Transformation
excerpt: >
  I started this journey trying to figure out how to earn online. While that's still a big part of it, I'm realizing that I just need to keep learning digital tools, and grow my skillset.
caption: #"[Web-work.tools](https://web-work.tools)"
img: "assets/img/digital-transformation-for-the-commons.jpg"
img2: "assets/img/webwork-tools.png"
tags: 
  - Jamstack
  - Content-Creation
  - SEO
  - Digital-Transformation
published: true
---

At the begining of 2019, I started a few lists on subjects related to working online. Web-publishing is one of those fundamental required skills for most any type of work online. As a writer, I quickly realized that I'd be better off creating content for my own websites, than selling it to someone who intended to make a lot more money with it than they would pay me.

[Web-work.tools](https://web-work.tools) is where I save information for my own personal reference, and anyone else who's interested. 

[![image]({{ page.img2 | relative_url }})](https://web-work.tools)

## Digital Transformation for the Commons

Since the explosion of the web, traditional enterprise has found itself continually under fire. Every executive worth their salt knows that anyone with an internet connection has the potential to decimate their entire industry, with some new disintermediating product or service.

Recall, that some of the largest corporations existing today were built in a garage.

The enterprise has considerable resources devoted to undergoing their own digital transformation, to distrupt their own business models before someone else does it for them.

Now, the independent would-be worker of the web isn't likely to know all of this. When I began, all I knew is that it's clearly possible to work online, not only that, it's practically inevitable. 

Today there is an endless array of useful tools and apps that you and I can learn to create value with. You don't need money, you just need time, a computer and an internet connection. 

## Featured Content
* [Github Pages Starter Pack](https://web-work.tools/jamstack/github-pages-starter-pack/)
  * [Github Pages Extended Resources](https://web-work.tools/jamstack/github-pages-extended-resources/)
* [Hugo Starter Kit](https://web-work.tools/jamstack/hugo-starter-kit/)
* [Learn SEO](https://web-work.tools/seo/learn/)
  * [100's of Mostly Free SEO Tools](https://web-work.tools/seo/tools/)


### [Indieweb Curated](https://github.com/web-work-tools/indieweb)
* [IndieWeb Resources](https://web-work.tools/indieweb/resources/)
* [IndieWeb GitHub Repositories](https://web-work.tools/indieweb/github-repos/)
* [FreeNode IRC #indieweb-dev on IIW RWoT and DID's](https://web-work.tools/indieweb/indieweb-dev-on-did/)
* [Indigo Authors - Indiweb Features](https://web-work.tools/indieweb/indigo-authors/)

Indieweb is all about taking control of your content, and all kinda stuff I'm just beginning to grok.
